import {BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import List from "./pages/list/List";
import { userInputs } from "./formSource";
import ProductList from "./pages/list/ProductList";
import Complaintlist from "./pages/list/Complaintlist";
import Feedbacklist from "./pages/list/Feedbacklist";
import NewUser from "./pages/new/NewUser";

function App() {


  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" >
            <Route index element={<Login/>}/>
            <Route path="home"
              element={
                  <Home/>
          
              }
            />
            <Route path="users">
              <Route 
                index 
                element={               
                    <List/>      
                }
              />
              
           </Route>
           <Route path="products">
              <Route 
                index 
                element={
                
                    <ProductList/>
                  
                }
              />
               <Route 
                path="new" 
                element={   
                    <NewUser inputs={userInputs} title="Add New User"/>
                }
              />
           </Route>
           <Route path="complaint">
              <Route 
                index 
                element={
                
                    <Complaintlist/>
                  
                }
              />
           </Route>
          
           <Route path="feedback">
              <Route 
                index 
                element={
                
                    <Feedbacklist/>  
                }
              />
              </Route>
         </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
