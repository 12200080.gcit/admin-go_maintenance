export const userInputs = [
    {
        id: "name",
        label: "Name",
        type: "text",
        placeholder: ""
    },
    {
        id: "email",
        label: "Email",
        type: "email",
        placeholder: ""
    },
    {
        id: "password",
        label: "Password",
        type: "password",
        placeholder: ""
    },
    {
        id: "workfield",
        label: "Workfield",
        type: "text",
        placeholder: ""
    },
    {
        id: "Phonenumber",
        label: "Phone number",
        type: "number",
        placeholder: ""
    },
    {
        id: "Address",
        label: "Address",
        type: "text",
        placeholder: ""
    }
]

