import { initializeApp } from "firebase/app";
import { getAuth, signOut } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyAnQSVYclLnQRACDb2Jegi4CTCwPMhnUcY",
  authDomain: "gomaintenance-13e0c.firebaseapp.com",
  databaseURL: "https://gomaintenance-13e0c-default-rtdb.firebaseio.com",
  projectId: "gomaintenance-13e0c",
  storageBucket: "gomaintenance-13e0c.appspot.com",
  messagingSenderId: "64153975318",
  appId: "1:64153975318:web:83f60b6346c4313acf1337",
  
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth()
export const storage = getStorage(app);

export function logout(){
  signOut(auth);
}

