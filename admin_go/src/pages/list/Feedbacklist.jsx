import './list.scss'
import Navbar from '../../components/navbar/Navbar'
import Sidebar from '../../components/sidebar/Sidebar'
import Feedback from '../../components/datatable/Feedback'

const Feedbacklist = () => {
  return (
    <div className='list'>
      <Sidebar/>
      <div className="listContainer">
        <Navbar/>
        <Feedback/>
      </div>
    </div>
  )
}

export default Feedbacklist