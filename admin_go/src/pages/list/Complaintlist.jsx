import './list.scss'
import Navbar from '../../components/navbar/Navbar'
import Sidebar from '../../components/sidebar/Sidebar'
import Complaint from '../../components/datatable/Complaint'

const Complaintlist = () => {
  return (
    <div className='list'>
      <Sidebar/>
      <div className="listContainer">
        <Navbar/>
        <Complaint/>
      </div>
    </div>
  )
}

export default Complaintlist