import { useState } from 'react'
import './login.scss'
import {  signInWithEmailAndPassword  } from "firebase/auth";
import { auth } from '../../firebase';
import { useNavigate } from 'react-router-dom';

const Login = () => {
  const [error, setError] = useState(false)
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  
  const navigate = useNavigate()
  
  const handleLogin = (e)=>{
    e.preventDefault()

    signInWithEmailAndPassword (auth, email, password)
      .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;
        navigate("home")
      })
      .catch((error) => {
       // const errorMessage = error.message;
        setError(error);
      });
  }
  return (
      <div className="login">
          <form onSubmit={handleLogin}>
          <img mode='center' src='images/logo.png'/>
          <h4>Admin Login</h4>
            <input type="email" placeholder='Email' onChange={e=>setEmail(e.target.value)}/>
            <input type="password"  placeholder='Password' onChange={e=>setPassword(e.target.value)}/>
            <button type='submit'>Login</button>
           {error && <span>Email or password can't be empty!</span>}
          </form>
      </div>
  )
}

export default Login