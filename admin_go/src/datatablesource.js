export const userColumns = [
  { field: 'id', headerName: 'ID', width: 100 },
  { field: 'name', headerName: 'Username', width: 120 },
  { field: 'email', headerName: 'Email', width: 200 },
  { field: 'Phonenumber', headerName: 'Phone Number', width: 150 },
  { field: 'Address', headerName: 'Address', width: 100 },
  // { field: 'Blocknumber', headerName: 'Block Number', width: 120 },
  { field: 'Housenumber', headerName: 'House Number', width: 120 },
];
  
  export const productColumns = [
    { field: 'id', headerName: 'ID', width: 100 },
    { 
        field: 'img', 
        headerName: 'Certificate', 
        width: 200, 
        renderCell: (params)=>{
            return (
              <div className="cellWithImg">
                <file src={params.row.img} alt="" className="cellImg" />
                {params.row.product}
              </div>
      )
    }},
    { field: 'name', headerName: 'Name', width: 150 },
    { field: 'email', headerName: 'Email', width: 200 },
    { field: 'workfield', headerName: 'WorkField', width: 150 },
    { field: 'Phonenumber', headerName: 'Phone Number', width: 150 },
    { field: 'Address', headerName: 'Address', width: 150 },
    
  ];

  export const complaintColumns = [
    { field: 'id', headerName: 'ID', width: 100 },
    { field: 'name', headerName: 'Name', width: 150 },
    { field: 'Address', headerName: 'Address', width: 150 },
    { field: 'Housenumber', headerName: 'HouseNumber', width: 150 },
    { field: 'Blocknumber', headerName: 'BlockNumber', width: 150 },
    { field: 'Phonenumber', headerName: 'PhoneNumber', width: 150 },
    { field: 'complaint', headerName: 'Complaint', width: 250 },
    { field: 'rate', headerName: 'Charges', width: 150 },
  ];

  export const feedbackColumns = [
    { field: 'id', headerName: 'ID', width: 100 },
    { field: 'feedback', headerName: 'Feedback', width: 350 },
  ];

    
  


  

  