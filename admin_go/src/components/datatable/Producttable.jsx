import './producttable.scss'
import { DataGrid } from '@mui/x-data-grid';
import { productColumns} from '../../datatablesource';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { collection, doc, deleteDoc, onSnapshot  } from "firebase/firestore";
import {db} from "../../firebase";

const Producttable = () => {
  const [data, setData] = useState([]);

  useEffect(()=> {
     const unsub = onSnapshot(
      collection(db, "MaintenanceUser"),
      (snapShot) => {
        let list = [];
        snapShot.docs.forEach((doc) => {
          list.push({ id: doc.id, ...doc.data() });
        });
        setData(list);
      },
      (error) => {
        console.log(error); 
      }
    );

    return () => {
      unsub();
    };
  }, []);

  const handleDelete = async (id) => {
    try{
      await deleteDoc(doc(db, "MaintenanceUser", id));
      setData(data.filter((item)=> item.id !== id));
    } catch(err){
      console.log(err)
    }
  }
  const actionColumn = [
    { 
      field: "action", 
      headerName: "Action", 
      width: 200, 
      renderCell:(params)=>{
        return (
          <div className="cellAction">
            {/* <Link to='/products/test' style={{ textDecoration: "none" }}>
            <div className="viewButton">Accept</div>
            </Link> */}
            <div 
              onClick={()=> handleDelete(params.row.id)} 
              className="deleteButton"
              >
                Delete
            </div>
          </div>
        );
      },
    },
  ];
      return (
        <div className='datatable'>
          <div className="datatableTitle">
            <Link 
              to='/products/new' 
              className='link'
            >
            Add New User
            </Link>
          </div>
            <DataGrid
              rows={data}
              columns={productColumns.concat(actionColumn)}
              pageSize={7}
              rowsPerPageOptions={[7]}
              checkboxSelection
            />
        </div>
      )
    }

    export default Producttable