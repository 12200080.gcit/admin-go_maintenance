import './datatable.scss'
import { DataGrid } from '@mui/x-data-grid';
import {  complaintColumns } from '../../datatablesource';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { collection, getDocs, doc, deleteDoc, onSnapshot  } from "firebase/firestore";
import {db} from "../../firebase";

const Complaint = () => {
  const [data, setData] = useState([]);

  useEffect(()=> {
  
     const unsub = onSnapshot(
      collection(db, "Filecomplaint"),
      (snapShot) => {
        let list = [];
        snapShot.docs.forEach((doc) => {
          list.push({ id: doc.id, ...doc.data() });
        });
        setData(list);
      },
      (error) => {
        console.log(error);
      }
    );

    return () => {
      unsub();
    };
  }, []);

  const handleDelete = async (id) => {
    try{
      await deleteDoc(doc(db, "Filecomplaint", id));
      setData(data.filter((item)=> item.id !== id));
    } catch(err){
      console.log(err)
    }
  }
  const actionColumn = [
    { 
      field: "action", 
      headerName: "Action", 
      width: 150, 
      renderCell:(params)=>{
        return (
          <div className="cellAction">
            {/* <Link to='/Filecomplaint/test' style={{ textDecoration: "none" }}>
            <div className="viewButton">View</div>
            </Link> */}
            <div 
              onClick={()=> handleDelete(params.row.id)} 
              className="deleteButton"
              >
                Delete
            </div>
          </div>
        );
      },
    },
  ];
      return (
        <div className='datatable'>
          <div className="datatableTitle">
            {/* <Link 
              to='/User/new' 
              className='link'
            >
            Add New User
            </Link> */}
          </div>
            <DataGrid
              rows={data}
              columns={complaintColumns.concat(actionColumn)}
              pageSize={7}
              rowsPerPageOptions={[7]}
              checkboxSelection
            />
        </div>
      )
    }

    export default Complaint