import './sidebar.scss'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import EngineeringIcon from '@mui/icons-material/Engineering';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import FeedbackIcon from '@mui/icons-material/Feedback';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import {Link, useNavigate } from "react-router-dom"
import { logout } from '../../firebase';

const Sidebar = () => {
    let navigate = useNavigate();
    async function handleLogOut() {
      logout();
      navigate("/");
    }
  
  return (
    <div className='sidebar'>
        <div className="top">
            {/* <Link to='/' style={{textDecoration: "none"}}> */}
                <span className="logo">Admin Dashboard</span>
            {/* </Link> */}
        </div>
        <hr />
        <div className="bottom">
            <ul>
                <Link to='/users' style={{textDecoration: "none"}}>
                <li>
                    <PersonOutlineIcon className='icon'/>
                    <span>Resident</span>
                </li>
                </Link>
            </ul>

            <ul>
                <Link to='/products' style={{textDecoration: "none"}}>
                <li>
                    <EngineeringIcon className='icon'/>
                    <span>Maintenance</span>
                </li>
                </Link>
            </ul>

            <ul>
                <Link to='/complaint' style={{textDecoration: "none"}}>
                <li>
                    <CreditCardIcon className='icon'/>
                    <span>File Complaint</span>
                </li>
                </Link>
            </ul>

            <ul>
            <Link to='/feedback' style={{textDecoration: "none"}}>
                <li>
                    <FeedbackIcon className='icon'/>
                    <span>Feedback</span>
                </li>
            </Link>
            </ul>

            <ul>
            <Link to="/" style={{ textDecoration: "none" }}>
            <li>
              <ExitToAppIcon className="icon" />
                <span>Logout</span>

                </li>
              {/* <span>
                <button onClick={handleLogOut}>LogOut</button>
              </span> */}
          </Link>

            </ul>

        </div>
    </div>
  )
}

export default Sidebar