import './navbar.scss'

const Navbar = () => {
  return (
    <div className='navbar'>
      <div className="wrapper">
        <div className="items">
          <div className="item">
            <h3>Go Maintenance</h3>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Navbar